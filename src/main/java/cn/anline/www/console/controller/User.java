package cn.anline.www.console.controller;

import act.controller.annotation.UrlContext;
import cn.anline.www.console.bean.UserTest;
import com.alibaba.fastjson.JSONPObject;
import org.osgl.mvc.annotation.GetAction;
import org.osgl.mvc.annotation.PostAction;

@UrlContext("user")
public class User extends ConsoleBaseController {

    @GetAction({"index",""})
    public void index(){
        tpl(_ThemePath()+"user/index");
    }

}
