**安浪创想开发的人工智能官网管理平台**

**本系统免费开源，仅供非商业用户使用，如果需要商业用途，需要获得安浪创想的许可**

**分为三端：**

前台官网模块(Home)+用户控制台(Console)+系统后台(Admin)

**使用技术栈：**

JAVA8+Act.Framework+MongoDb+MySQL+AngularJS(BlurAdmin)+Bootstrap（三端都在用）+JQuery

##开发者：

**作者：Jiankian(绿血贵族)**

**QQ:51412708**

**微信：jiankian**


**团队：安浪创想(Anline)**

**安浪创想官网:[http://www.anline.cn](http://www.anline.cn)**

**程序最新开源仓库地址为：码云平台**

码云：[https://gitee.com/jiankian/AI-OpenAPI-CMS.git](https://gitee.com/jiankian/AI-OpenAPI-CMS.git)

**安装**

克隆到本地导入IDEA，找到mian函数位置直接启动即可。

 把mongodb数据文件夹里的数据导入到MongoDB
 先启动MongoDB才能启动程序。

精力有限，只开源了基础框架，不开源那些做了OCR接口、开发快硬件接口。和OpenCV java接口。
用服务器端java来运行任何java和python库然后直接返回到HTTP端口。任何Object自动转换json格式。也可以web格式，文件类型直接InputStream类型。

**打包**

`mvn clean package`

**配合DeepLearning4J**

查看文档：
[https://skymind.readme.io/v1.0.1/docs/installation](https://skymind.readme.io/v1.0.1/docs/installation)
与任何普通java项目可以搭配合作。无法与java servlet搭配。

返回普通对象 `Object`类型 自动输出json数据到您的任何平台对接
内置`JWT`了，可以`jwt token`作为身份验证

**AI官网** 
[http://ai.anline.cn](http://ai.anline.cn) （演示地址配置低，服务器跑不动会不稳定）

后台：

[/admin](http://ai.anline.cn/admin)

![](/screenshot/Jietu20170908-055455@2x.jpg)
![](/screenshot/Jietu20170908-055504@2x.jpg)
![](/screenshot/Jietu20170908-055519@2x.jpg)